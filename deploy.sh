#!/usr/bin/env bash
mkdir -p ~/.config/systemd/user
cp take-shot.{service,timer} ~/.config/systemd/user/
systemctl enable --user --now take-shot.timer
loginctl enable-linger $USER
